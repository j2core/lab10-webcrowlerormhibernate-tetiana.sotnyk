package com.j2core.sts.webcrawler.hibernate.dao.hibernate;

import com.j2core.sts.webcrawler.hibernate.dao.hibernate.exception.DBException;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.NodeData;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.ResultingInformation;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.UrlsInformation;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Interface for work with DB
 */
public interface WorkerDB {

    Integer nodeId = null;

    /**
     * The method get URLs for process
     *
     * @param amountUrl      amount URLs
     * @return   collection with URLs for process
     */
    List<UrlsInformation> getUrlInformation(int amountUrl, int nodeId);

    /**
     * The method save last information in to the DB
     *
     * @param analysedPages    collection with analysed URL's information
     * @param processesLink    collection with URLs stopped in process
     * @param pagesLink        collection with URLs for process
     */
    void finalSaveInformation(BlockingQueue<ResultingInformation> analysedPages, BlockingQueue<UrlsInformation> processesLink, BlockingQueue<UrlsInformation> pagesLink, int nodeId);

    /**
     * The method add URL's information in to DB
     *
     * @param resultingInformation            URL's information
     * @return   add information in DB successfully or no
     * @throws DBException if this URL not exist
     */
    boolean addInformation(ResultingInformation resultingInformation) throws DBException;


    /**
     * The method added information about this node
     *
     * @param name      node's name
     * @return          node's information
     */
    Integer addNode(String name);


    /**
     * The method change node's status
     *
     * @param nodeData     node's information
     */
    void stopNode(NodeData nodeData);


    /**
     * The method returned deprecate data to the DB for work other node
     *
     * @return returned information in DB successfully or no
     */
    boolean returnDeprecateData( int amountLink);

}
