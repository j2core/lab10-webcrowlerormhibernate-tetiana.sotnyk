package com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sts on 12/1/16.
 */
@Entity
@Table(name = "nodeData")
public class NodeData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "nodeId")
    private int nodeId;

    @Column(name = "nodeName", length = 65535)
    private String nodeName;

    @Column(name = "status")
    private boolean status = true;

    @Column(name = "startUnixTime", length = 65535)
    private long startUnixTime;

    @Column(name = "stopUnixTime", length = 65535)
    private long stopUnixTime;

    public NodeData(){

    }

    public NodeData (String name){
        this.nodeName = name;
        this.status = true;
        this.startUnixTime = System.currentTimeMillis();
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public boolean isStatusWork() {
        return status;
    }

    public void setStatusWork(boolean statusWork) {
        this.status = statusWork;
    }

    public long getStartTime() {
        return startUnixTime;
    }

    public void setStartTime(long startUnixTime) {
        this.startUnixTime = startUnixTime;
    }

    public long getStopTime() {
        return stopUnixTime;
    }

    public void setStopTime(long stopUnixTime) {
        this.stopUnixTime = stopUnixTime;
    }

}
