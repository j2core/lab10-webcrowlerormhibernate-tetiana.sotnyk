package com.j2core.sts.webcrawler.hibernate.dao.hibernate.exception;

/**
 * This class exception signals if not found URL's id
 */
public class NotFoundIdURLException extends DBException{

    private static String message = "Not found URL's id!";
    private String url;

    /**
     * Default constructor
     */
    public NotFoundIdURLException(){
        super();
    }

    /**
     * Constructor NotFoundIdURLException with value not found Url
     * @param url value not found's URL
     */
    public NotFoundIdURLException(String url){
        super(message);
        this.url = url;

    }

    public String getUrl(){
        return url;
    }

}
