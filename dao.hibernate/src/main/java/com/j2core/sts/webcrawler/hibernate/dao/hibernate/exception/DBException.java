package com.j2core.sts.webcrawler.hibernate.dao.hibernate.exception;

/**
 * Created by sts on 7/4/16.
 */

/**
 * Class exception for problems with DB
 */
public class DBException extends Exception {

    private static String message = "You have problems with your DB!"; // default exception message

    /**
     * Default constructor DBException's class
     */
    public DBException(){
        super(message);
    }

    /**
     * Constructor DBException's with extra information
     * @param information extra information about exception
     */
    public DBException(String information){

        super(message + information);

    }

}
