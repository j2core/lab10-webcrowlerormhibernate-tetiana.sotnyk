package com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto;

import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.URLStatus;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sts on 10/20/16.
 */
@Entity
@Table(name = "urlData")
public class UrlData implements Serializable{

    private static final long serialVersionUID = 40L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "urlId")
    private int urlId;

    @Column(name = "url", unique = true, length = 65535)
    private String url;

    @Column(name = "amountTransition")
    private int amountTransition;

    @Column(name = "status")
    private URLStatus status;

    @Column(name = "statusChangeTime")
    private long statusChangeTime;

    @OneToOne
    @JoinColumn(name = "NodeId")
    private NodeData nodeData;

    public UrlData(){

    }

    public UrlData(String url, int amountTransition, URLStatus status) {
        this.url = url;
        this.amountTransition = amountTransition;
        this.status = status;
        this.statusChangeTime = System.currentTimeMillis();
    }

    public int getUrlId() {
        return urlId;
    }

    public void setUrlId(int urlId) {
        this.urlId = urlId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getAmountTransition() {
        return amountTransition;
    }

    public void setAmountTransition(int amountTransition) {
        this.amountTransition = amountTransition;
    }

    public URLStatus getStatus() {
        return status;
    }

    public void setStatus(URLStatus status) {
        this.status = status;
    }

    public long getStatusChangeTime() {
        return statusChangeTime;
    }

    public void setStatusChangeTime(long statusChangeUnixTime) {
        this.statusChangeTime = statusChangeUnixTime;
    }

    public NodeData getNodeData() {
        return nodeData;
    }

    public void setNodeData(NodeData nodeData) {
        this.nodeData = nodeData;
    }

    @Override
    public String toString() {
        return "UrlData{" +
                "urlId=" + urlId +
                ", url='" + url + '\'' +
                ", amountTransition=" + amountTransition +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UrlData urlData = (UrlData) o;

        if (urlId != urlData.urlId) return false;
        if (amountTransition != urlData.amountTransition) return false;
        if (!url.equals(urlData.url)) return false;
        return status == urlData.status;

    }

    @Override
    public int hashCode() {
        int result = urlId;
        result = 31 * result + url.hashCode();
        result = 31 * result + amountTransition;
        return result;
    }
}
