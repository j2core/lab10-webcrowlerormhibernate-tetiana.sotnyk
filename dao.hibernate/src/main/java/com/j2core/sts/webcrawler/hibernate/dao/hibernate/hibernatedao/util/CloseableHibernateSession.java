package com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.util;

import org.hibernate.Session;

/**
 * Created by sts on 1/5/17.
 */
public class CloseableHibernateSession implements AutoCloseable{

    private final Session session;

    public CloseableHibernateSession(Session session) {
        this.session = session;
    }

    @Override
    public void close() throws Exception {

        if (session != null) {
            session.close();
        }
    }

    public Session getSession(){
        return session;
    }
}
