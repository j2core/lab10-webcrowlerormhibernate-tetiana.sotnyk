package com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao;

import com.google.common.collect.Multiset;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.WorkerDB;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.exception.DBException;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.NodeData;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.PageInformation;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.UrlData;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.WordInformation;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.util.CloseableHibernateSession;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.util.DBSessionProvider;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.ResultingInformation;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.URLStatus;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.UrlsInformation;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.*;
import java.util.concurrent.BlockingQueue;

/**
 * Class for work with DB and use JPA annotation
 */
public class HibernateWorkerDB implements WorkerDB {


    private final static Logger LOGGER = Logger.getLogger(HibernateWorkerDB.class); // class for save logs information
    private int amountMillisecondDeprecate;

    /**
     * Constructor
     */
    public HibernateWorkerDB(int amountMillisecondDeprecate){

        this.amountMillisecondDeprecate = amountMillisecondDeprecate;

    }


    @Override
    public List<UrlsInformation> getUrlInformation(int amountLink, int nodeId) {

        Transaction transaction;
        List<UrlData> dataList;
        List<UrlsInformation> result = new LinkedList<>();

        try (CloseableHibernateSession hibernateSession =  new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = hibernateSession.getSession().beginTransaction();

            Criteria criteria = hibernateSession.getSession().createCriteria(UrlData.class);
            criteria.add(Restrictions.eq("status", URLStatus.NOT_PROCESSED)).setMaxResults(amountLink);

            dataList =  (List<UrlData>) criteria.list();

            NodeData nodeData = new NodeData();
            nodeData.setNodeId(nodeId);

            for (UrlData urlData : dataList) {

                urlData.setNodeData(nodeData);
                urlData.setStatusChangeTime(System.currentTimeMillis());
                urlData.setStatus(URLStatus.PROCESSES);

                hibernateSession.getSession().update(urlData);

                UrlsInformation information = new UrlsInformation(urlData.getUrlId(), urlData.getUrl(), urlData.getAmountTransition(), nodeId);
                information.setStatus(URLStatus.PROCESSES);
                result.add(information);

            }

            transaction.commit();

        }catch (Exception ex){
            LOGGER.error(ex);
        }

        return result;
    }


    @Override
    public void finalSaveInformation(BlockingQueue<ResultingInformation> analysedPages, BlockingQueue<UrlsInformation> processesLink,
                                     BlockingQueue<UrlsInformation> pagesLink, int nodeId) {

        if (!analysedPages.isEmpty()){

            for (ResultingInformation resultingInformation : analysedPages) {
                try {
                    if (!addInformation(resultingInformation)){
                        processesLink.add(new UrlsInformation(resultingInformation.getUrlId(), resultingInformation.getPageUrl(),
                                resultingInformation.getAmountTransition(), resultingInformation.getNodeId()));
                    }
                } catch (DBException e) {
                    e.printStackTrace();
                }
            }

        }

        if (!processesLink.isEmpty()) {
            pagesLink.addAll(processesLink);
        }
        if (!pagesLink.isEmpty()) {

            List<UrlData> lastData = changeCollection(pagesLink);
            lastData = equalsNodeId(lastData, nodeId);
            changeUrlData(URLStatus.NOT_PROCESSED, lastData);

        }
    }


    /**
     * The method do equal with work node's id and node's id witch wrote in URL's data
     *
     * @param lastData   URL's data witch need equals
     * @param id         work node's id
     * @return  collection UrlData with work node's id
     */
    public List<UrlData> equalsNodeId(List<UrlData> lastData, int id) {

        try (CloseableHibernateSession hibernateSession =  new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Iterator<UrlData> urlDataIterator = lastData.iterator();

            while (urlDataIterator.hasNext()) {

                UrlData data = urlDataIterator.next();
                UrlData dbData = (UrlData) hibernateSession.getSession().get(UrlData.class, data.getUrlId());

                if (id != dbData.getNodeData().getNodeId()) {
                    urlDataIterator.remove();
                }
            }

        } catch (Exception e) {
            LOGGER.error(e);
        }
        return lastData;
    }


    @Override
    public boolean addInformation(ResultingInformation resultingInformation) throws DBException {

        Transaction transaction;

        try (CloseableHibernateSession hibernateSession =  new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            UrlData urlData = (UrlData) hibernateSession.getSession().get(UrlData.class, resultingInformation.getUrlId());

            if (urlData.getNodeData().getNodeId() == resultingInformation.getNodeId()) {

                transaction = hibernateSession.getSession().beginTransaction();

                PageInformation pageInformation = new PageInformation(resultingInformation.getPagesText(), new Date(), urlData);

                Criteria criteria = hibernateSession.getSession().createCriteria(PageInformation.class).add(Restrictions.eq("urlData", urlData));

                PageInformation page = (PageInformation) criteria.uniqueResult();

                if (page == null) {
                    int pageId = (int) hibernateSession.getSession().save(pageInformation);
                    pageInformation.setPageId(pageId);
                }

                addWordInformation(hibernateSession, urlData, pageInformation, resultingInformation.getWordsInPage());

                addNewUrlInformation(hibernateSession, resultingInformation.getUrlCollectionNew());

                changeUrlData(hibernateSession, URLStatus.PROCESSED, resultingInformation.getUrlId());

                transaction.commit();

            }else throw new Exception(" This URL do not belong this node");

        }catch (Exception ex){

            LOGGER.error(ex);
            return false;

        }

        return true;

    }


    /**
     * The method change URL's status and change status time
     *
     * @param urlStatus         URL's status
     * @param dataList          collection with URL data
     * @return change URL's status successfully or no
     */
    public boolean changeUrlData(URLStatus urlStatus, List<UrlData> dataList) {

        Transaction transaction;

        try (CloseableHibernateSession hibernateSession =  new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = hibernateSession.getSession().beginTransaction();

            for (UrlData data : dataList) {

                data.setStatusChangeTime(System.currentTimeMillis());
                data.setStatus(urlStatus);
                hibernateSession.getSession().update(data);

            }

            transaction.commit();

        }catch (Exception ex){
            LOGGER.error(ex);
            return false;
        }

        return true;

    }


    /**
     * The method change URL's status and change status time
     *
     * @param hibernateSession  session for work with DB
     * @param urlStatus         URL's status
     * @param urlId             URL's id
     * @return change URL's status successfully or no
     */
    public boolean changeUrlData(CloseableHibernateSession hibernateSession, URLStatus urlStatus, int urlId) {

        try {

            UrlData data = (UrlData) hibernateSession.getSession().get(UrlData.class, urlId);

            data.setStatus(urlStatus);
            data.setStatusChangeTime(System.currentTimeMillis());

            hibernateSession.getSession().update(data);

        } catch (Exception ex) {
            LOGGER.error(ex);
            return false;
        }

        return true;

    }


    /**
     * The method change collection with UrlInformation to collection with UrlData
     *
     * @param pagesLink      collection with UrlInformation
     * @return  collection with UrlData
     */
    private List<UrlData> changeCollection(BlockingQueue<UrlsInformation> pagesLink){

        List<UrlData> result = new LinkedList<>();

        for (UrlsInformation urlsInformation : pagesLink){

            UrlData urlData = new UrlData(urlsInformation.getPagesUrl(), urlsInformation.getAmountTransition(), urlsInformation.getStatus());
            urlData.setUrlId(urlsInformation.getId());

            result.add(urlData);

        }

        return result;
    }


    /**
     * The method add word information from page to the DB
     *
     * @param hibernateSession       session for work with DB
     * @param urlData                URL's data
     * @param pageData               page's data
     * @param wordsInPage            collection with words from page
     * @return add word in DB successfully or no
     */
    public boolean addWordInformation(CloseableHibernateSession hibernateSession, UrlData urlData, PageInformation pageData, Multiset<String> wordsInPage){

        try{
            Set<Multiset.Entry<String>> wordsCollection = wordsInPage.entrySet();

            for (Multiset.Entry<String> word : wordsCollection){

                WordInformation wordData = new WordInformation(urlData, pageData, word.getElement(), word.getCount());

                hibernateSession.getSession().save(wordData);

            }

        }catch (Exception e){

            LOGGER.error(e);
            return false;
        }

        return true;
    }


    /**
     * The method add new URL in the DB
     *
     * @param hibernateSession      session for work with DB
     * @param newUrlInformation     collection with new URLs
     * @return add new URLs in DB successfully or no
     */
    public boolean addNewUrlInformation(CloseableHibernateSession hibernateSession, Set<UrlsInformation> newUrlInformation) {

        UrlData urlData;

        try {

            for (UrlsInformation data : newUrlInformation) {

                urlData = new UrlData(data.getPagesUrl(), data.getAmountTransition(), URLStatus.NOT_PROCESSED);

                Criteria criteria = hibernateSession.getSession().createCriteria(UrlData.class).add(Restrictions.eq("url", data.getPagesUrl()));

                UrlData url = (UrlData) criteria.uniqueResult();

                if (url == null) {
                    hibernateSession.getSession().save(urlData);
                }
            }

        } catch (Exception e) {

            LOGGER.error(e);
            return false;
        }

        return true;
    }


    @Override
    public Integer addNode(String nodeName){

        Transaction transaction;
        NodeData nodeData = new NodeData(nodeName);
        int nodeId = -1;

        try(CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = hibernateSession.getSession().beginTransaction();

            nodeId = (Integer) hibernateSession.getSession().save(nodeData);

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return nodeId;
    }


    @Override
    public void stopNode(NodeData nodeData) {

        nodeData.setStatusWork(false);
        nodeData.setStopTime(System.currentTimeMillis());

        Transaction transaction;

        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = hibernateSession.getSession().beginTransaction();

            nodeData.setStatusWork(false);
            nodeData.setStopTime(System.currentTimeMillis());

            hibernateSession.getSession().update(nodeData);

            transaction.commit();

        }catch (Exception ex){
            LOGGER.error(ex);
        }
    }


    /**
     * The method return deprecate data in to work
     * @return  return date successfully or no
     */
    public boolean returnDeprecateData(int amountLink){

        List<UrlData> dataList;
        Transaction transaction;

        long unixTime = System.currentTimeMillis();
        long oldUnixTime = unixTime - amountMillisecondDeprecate;

        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = hibernateSession.getSession().beginTransaction();

            Criteria criteria = hibernateSession.getSession().createCriteria(UrlData.class);
            criteria.add(Restrictions.le("statusChangeTime", oldUnixTime)).setMaxResults(amountLink);

            dataList =  criteria.list();

            for (UrlData urlData : dataList){

                urlData.setNodeData(null);
                urlData.setStatus(URLStatus.NOT_PROCESSED);
                urlData.setStatusChangeTime(System.currentTimeMillis());

                hibernateSession.getSession().update(urlData);

            }

            transaction.commit();

        }catch (Exception ex){
            LOGGER.error(ex);
            return false;
        }

        return true;
    }

}
