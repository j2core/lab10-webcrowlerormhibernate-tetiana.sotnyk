package com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao;

import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.util.DBSessionProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by sts on 1/6/17.
 */
public class DBSessionProviderTest {

    @Test
    public void testGetProviderInstance(){

        Assert.assertNotNull(DBSessionProvider.getProviderInstance());

    }


    @Test
    public void testGetSessionFactory() throws Exception {

        Assert.assertNotNull(DBSessionProvider.getProviderInstance().getSessionFactory());

    }
}
