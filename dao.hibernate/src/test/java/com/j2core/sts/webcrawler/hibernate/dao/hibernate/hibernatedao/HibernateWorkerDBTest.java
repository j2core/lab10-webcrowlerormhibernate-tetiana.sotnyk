package com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.exception.DBException;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.NodeData;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.PageInformation;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.UrlData;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.dto.WordInformation;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.util.CloseableHibernateSession;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.util.DBSessionProvider;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.ResultingInformation;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.URLStatus;
import com.j2core.sts.webcrawler.hibernate.jsoupinteraction.hibernate.dto.UrlsInformation;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;


/**
 * Created by sts on 11/4/16.
 */
public class HibernateWorkerDBTest {


    @BeforeClass
    public void createFirstData(){

        Transaction transaction;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            List<UrlData> firstData = new LinkedList<>();

            firstData.add(new UrlData("https://en.wikipedia.org/wiki/Spring_Framework", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("http://j2w.blogspot.com", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("http://docs.oracle.com/javase/7/docs/technotes/tools/windows/classpath.html", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("https://en.wikipedia.org/wiki/Universally_unique_identifier", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("http://community.actian.com/wiki/SQL_BOOLEAN_type", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("https://www.blueapron.com", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("https://www.wellsfargo.com", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("http://www.javaworld.com", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("https://docs.oracle.com", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("http://www.zpub.com/sf/history/", 1, URLStatus.NOT_PROCESSED));
            firstData.add(new UrlData("http://www.goodreads.com/genres/history", 1, URLStatus.NOT_PROCESSED));

            transaction = closeableSession.getSession().beginTransaction();

            for (UrlData url : firstData){

                closeableSession.getSession().save(url);

            }

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testAddNode(){

        HibernateWorkerDB hibernateWorkerDB = new HibernateWorkerDB(50000);
        NodeData nodeData = null;
        String nodeName = "Test node1";

        int nodeId = hibernateWorkerDB.addNode(nodeName);

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance()
                .getSessionFactory().openSession())){

            Criteria criteria = closeableSession.getSession().createCriteria(NodeData.class);
            criteria.add(Restrictions.eq("nodeId", nodeId)).setMaxResults(1);

            nodeData = (NodeData) criteria.list().get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals(nodeData.getNodeName(), nodeName);

    }


    @Test
    public void testStopNode(){

        HibernateWorkerDB hibernateWorkerDB = new HibernateWorkerDB(50000);
        String nodeName = "Test node2";
        List stoppedNode = null;

        int nodeId = hibernateWorkerDB.addNode(nodeName);

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance()
                .getSessionFactory().openSession())){

            Criteria criteria = closeableSession.getSession().createCriteria(NodeData.class);
            criteria.add(Restrictions.eq("nodeId", nodeId)).setMaxResults(1);

            NodeData nodeData = (NodeData) criteria.list().get(0);

            hibernateWorkerDB.stopNode(nodeData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance()
                .getSessionFactory().openSession())){

            Criteria criteria = closeableSession.getSession().createCriteria(NodeData.class);
            criteria.add(Restrictions.eq("status", false)).setMaxResults(1);

            stoppedNode = criteria.list();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(stoppedNode.size() == 1);

    }


    @Test
    public void testGetUrlInformation() {

        HibernateWorkerDB hibernateWorkerDB = new HibernateWorkerDB(50000);
        int amountUrl = 3;
        Integer nodeId = hibernateWorkerDB.addNode("Test node3");

        List dataList = hibernateWorkerDB.getUrlInformation(amountUrl, nodeId);

        Assert.assertTrue(dataList.size() == amountUrl);

    }


    public ResultingInformation createResultInformation(UrlData urlData) {

        ResultingInformation resultingInformation = new ResultingInformation(urlData.getUrlId(), urlData.getUrl(), urlData.getAmountTransition(), 1);

        Set<UrlsInformation> newUrl = new HashSet<>();
        newUrl.add(new UrlsInformation(-1, "http://stackoverflow.com/post123/link/url/1234", 3, -1));

        Multiset<String> wordsInPage = HashMultiset.create();
        wordsInPage.add("good");

        resultingInformation.setUrlCollectionNew(newUrl);
        resultingInformation.setWordsInPage(wordsInPage);
        resultingInformation.setPagesText("JPA: How to get entity based on field value other than ID?jsbf,bsfr,bsrrkfmhbsmfbsmdhfb");

        return resultingInformation;
    }

    @Test
    public void testAddInformation() throws DBException {

        HibernateWorkerDB worker = new HibernateWorkerDB(50000);
        Integer nodeId = worker.addNode("Test node4");

        ResultingInformation information = null;
        UrlData urlData;
        List amountUrl = null;
        List amountPage = null;
        List amountWord = null;
        List processedUrl = null;
        List resultUrl = null;
        List resultPage = null;
        List resultWord = null;
        List resultProcessedUrl = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            UrlsInformation urlsInformation =  worker.getUrlInformation(1, nodeId).get(0);

            urlData = (UrlData) closeableSession.getSession().get(UrlData.class, urlsInformation.getId());

            information = createResultInformation(urlData);

            Criteria criteriaUrl = closeableSession.getSession().createCriteria(UrlData.class);
            amountUrl = criteriaUrl.list();

            Criteria criteriaPage = closeableSession.getSession().createCriteria(PageInformation.class);
            amountPage = criteriaPage.list();

            Criteria criteriaWord = closeableSession.getSession().createCriteria(WordInformation.class);
            amountWord = criteriaWord.list();

            Criteria criteriaProcessedUrl = closeableSession.getSession().createCriteria(UrlData.class);
            criteriaProcessedUrl.add(Restrictions.eq("status", URLStatus.PROCESSED));
            processedUrl = criteriaProcessedUrl.list();


        } catch (Exception e) {
            e.printStackTrace();
        }

         worker.addInformation(information);

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Criteria criteriaUrl = closeableSession.getSession().createCriteria(UrlData.class);
            resultUrl = criteriaUrl.list();

            Criteria criteriaPage = closeableSession.getSession().createCriteria(PageInformation.class);
            resultPage = criteriaPage.list();

            Criteria criteriaWord = closeableSession.getSession().createCriteria(WordInformation.class);
            resultWord = criteriaWord.list();

            Criteria criteriaProcessedUrl = closeableSession.getSession().createCriteria(UrlData.class);
            criteriaProcessedUrl.add(Restrictions.eq("status", URLStatus.PROCESSED));
            resultProcessedUrl = criteriaProcessedUrl.list();


        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue((amountUrl.size() + 1) == resultUrl.size() && (amountPage.size() + 1) == resultPage.size() &&
                (amountWord.size() + 1) == resultWord.size() && (processedUrl.size() + 1) == resultProcessedUrl.size());

    }


    @Test
    public void testChangeURLStatusSing() {

        HibernateWorkerDB worker = new HibernateWorkerDB(50000);
        UrlData urlData = null;

        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            Transaction transaction = hibernateSession.getSession().beginTransaction();

            Criteria criteria = hibernateSession.getSession().createCriteria(UrlData.class);
            criteria.add(Restrictions.eq("status", URLStatus.NOT_PROCESSED)).setMaxResults(1);

            urlData = (UrlData) criteria.list().get(0);

            worker.changeUrlData(hibernateSession, URLStatus.PROCESSES, urlData.getUrlId());

            transaction.commit();

            Criteria changedCriteria = hibernateSession.getSession().createCriteria(UrlData.class);
            changedCriteria.add(Restrictions.eq("urlId", urlData.getUrlId())).setMaxResults(1);

            urlData = (UrlData) changedCriteria.list().get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }



        Assert.assertTrue(urlData.getStatus() == URLStatus.PROCESSES);

    }


    @Test
    public void testChangeURLStatusList() {

        HibernateWorkerDB worker = new HibernateWorkerDB(50000);
        List<UrlData> urlData = null;
        List<UrlData> urlProcesses = null;
        List<UrlData> changeUrlProcesses = null;

        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            Criteria criteria = hibernateSession.getSession().createCriteria(UrlData.class);
            criteria.add(Restrictions.eq("status", URLStatus.NOT_PROCESSED)).setMaxResults(3);

            urlData = (List<UrlData>) criteria.list();

            Criteria criteriaEnable = hibernateSession.getSession().createCriteria(UrlData.class);
            criteriaEnable.add(Restrictions.eq("status", URLStatus.PROCESSES));

            urlProcesses = (List<UrlData>) criteriaEnable.list();

            worker.changeUrlData(URLStatus.PROCESSES, urlData);

        } catch (Exception e) {
            e.printStackTrace();
        }
        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            Criteria criteriaChange = hibernateSession.getSession().createCriteria(UrlData.class);
            criteriaChange.add(Restrictions.eq("status", URLStatus.PROCESSES));
            changeUrlProcesses = (List<UrlData>) criteriaChange.list();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(changeUrlProcesses.size() == (urlData.size() + urlProcesses.size()));

    }


    @Test
    public void testAddNewUrlInformation(){

        Set<UrlsInformation> newUrl = new HashSet<>();
        newUrl.add(new UrlsInformation(-1, "http://svnbook.red-bean.com/en/1.7", 3, -1));
        newUrl.add(new UrlsInformation(-1, "http://svnbook.red-bean.com/users", 2, -1));
        newUrl.add(new UrlsInformation(-1, "http://docs.oracle.com/javaee/6/tutorial/doc/bnbrg.html", 2, -1));
        List dataInDB = null;
        List dataList = null;
        HibernateWorkerDB worker = new HibernateWorkerDB(50000);

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Criteria criteriaEnable = closeableSession.getSession().createCriteria(UrlData.class);

            dataInDB = (List<UrlData>) criteriaEnable.list();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Transaction transaction = hibernateSession.getSession().beginTransaction();

            worker.addNewUrlInformation(hibernateSession, newUrl);

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Criteria criteriaEnable = closeableSession.getSession().createCriteria(UrlData.class);

            dataList = (List<UrlData>) criteriaEnable.list();


        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(dataList.size() == (dataInDB.size() + newUrl.size()));
    }


    @Test
    public void testAddWordInformation(){

        List dataList = null;
        Multiset<String> wordsInPage = HashMultiset.create();
        wordsInPage.add("Java");
        int result = 0;
        HibernateWorkerDB worker = new HibernateWorkerDB(50000);
        UrlData urlData = null;
        PageInformation pageInformation = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Transaction transaction = closeableSession.getSession().beginTransaction();
            urlData = (UrlData) closeableSession.getSession().get(UrlData.class, 5);
            pageInformation = new PageInformation("mhrbgmrbgmbgmebg,mbergmbdegmbegmnbaer, hjkhfgskfghkskgfj", new Date(), urlData);

            closeableSession.getSession().save(pageInformation);

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try (CloseableHibernateSession hibernateSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Criteria criteria = hibernateSession.getSession().createCriteria(WordInformation.class);
            result = criteria.list().size();

            Transaction transaction = hibernateSession.getSession().beginTransaction();

            worker.addWordInformation(hibernateSession, urlData, pageInformation, wordsInPage);

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            Criteria criteriaEnable = closeableSession.getSession().createCriteria(WordInformation.class);

            dataList =  criteriaEnable.list();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertTrue(dataList.size() == (result + 1));

    }

}
