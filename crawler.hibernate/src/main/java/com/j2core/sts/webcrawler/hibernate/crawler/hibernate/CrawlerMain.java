package com.j2core.sts.webcrawler.hibernate.crawler.hibernate;

import com.j2core.sts.webcrawler.hibernate.dao.hibernate.WorkerDB;
import com.j2core.sts.webcrawler.hibernate.dao.hibernate.hibernatedao.HibernateWorkerDB;
import org.apache.log4j.Logger;

/**
 * The main class for start web crawler
 */
public class CrawlerMain {


    private final static Logger LOGGER = Logger.getLogger(CrawlerMain.class);         // class for save logs information
    private final static int amountMilliSecondDeprecate = 60000;

    public static void main(String[] args) {

        WorkerDB workerDB1 = new HibernateWorkerDB( amountMilliSecondDeprecate);
        Coordinator coordinator = new Coordinator(10, 5, "Coordinator", workerDB1);
        Thread threadCoordinator = new Thread(coordinator);

        threadCoordinator.start();

        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }

        coordinator.setFlagStopCoordinator();
        LOGGER.info("stop coordinators");

        try {
            threadCoordinator.join();
        } catch (InterruptedException e) {
            LOGGER.error(" Sorry");
        }

        LOGGER.info("stop main");

    }
}
